import json
import pandas as pd
import boto3
import io
import csv
# import requests


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e


    custom_report = pd.read_csv("s3://team-4-backend/glue_data/custom_report.csv")
    json_converted_report = json.loads(custom_report.to_json())
    json_converted_report_summary = json.loads(custom_report.to_json(orient='records'))

    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,POST"
        },
        "body": json.dumps({
            "message": "custom databrew report",
            "custom_databrew_report": json_converted_report,
            "custom_databrew_report_summary": json_converted_report_summary
            # "location": ip.text.replace("\n", "")
        }),
    }
