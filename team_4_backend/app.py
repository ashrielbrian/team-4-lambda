import json
import pandas as pd
import boto3
import io
import json
# import requests


def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    BUCKET = 'team-4-backend'
    ga_glue_result = 'glue_data/ga_glue_result.json'
    custom_report = 'glue_data/custom_report_jan_may.json'

    s3 = boto3.client('s3')
    ga_bytes_buffer = io.BytesIO()
    cr_bytes_buffer = io.BytesIO()

    # alternatively
    # obj = s3.get_object(Bucket=bucket, Key=key)
    # j = json.loads(obj['Body'].read())
    s3.download_fileobj(Bucket=BUCKET, Key=ga_glue_result, Fileobj=ga_bytes_buffer)
    s3.download_fileobj(Bucket=BUCKET, Key=custom_report, Fileobj=cr_bytes_buffer)

    print('current bucket:', BUCKET)
    ga_glue_result = json.loads(ga_bytes_buffer.getvalue().decode())
    custom_report = json.loads(cr_bytes_buffer.getvalue().decode())

    return {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET,POST"
        },
        "body": json.dumps({
            "message": "Google Analytics Results",
            "ga_glue_result": ga_glue_result,
            "custom_report": custom_report
            # "location": ip.text.replace("\n", "")
        }),
    }
